# coding = UTF-8
import re, pandas as pd  # url 불러오는 라이브러리

def get_oie_report(linknumber):
    # format 메소드 이용하여 new_linknumber를 넣는 url를 만듦
    url = f'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid={linknumber}'
    # pd.read_html: html에서 table 태그만을 뽑아 이를 각각 데이터프레임(df)으로 만들어 하나의 리스트로 묶음
    # table = pd.read_html(url, flavor='lxml')  # flavor='lxml': 'html5lib not found' 오류를 미연에 방지
    report_df, totalhead_df = (None, None)

    try:
        table = pd.read_html(url, flavor='lxml')  # flavor='lxml': 'html5lib not found' 오류를 미연에 방지
    except:
        return report_df, totalhead_df

    # table[0]: 각 table 태그의 텍스트를 행에 하나씩 대응하여 만든 df
    if not any(table[0][0].str.contains('Total outbreaks')) or any(table[0][0].str.contains('Water type')):
        # 발생 건 없을 때 or 수생 동물 건일 때
        # report_df, totalhead_df = (pd.DataFrame(), pd.DataFrame())
        return report_df, totalhead_df
    ## Summary 박스
    summary_df = table[3].T  # T: 행 열 전환
    summary_header = summary_df.loc[0]  # Summary 데이터프레임(df)의 칼럼명으로 지정하기 위함
    summary_df = summary_df.loc[1:].rename(columns=summary_header)
    
    # 국가명, 원인체, 링크 번호
    country_name = table[0].loc[0, 0].split(',')[-1]
    agent = table[0].loc[0, 0].split(',')[0]
    summary_df['Country'] = [country_name]
    summary_df['Agent'] = [agent]
    summary_df['Link number'] = [linknumber]
    ## 발생 건 박스
    # table[0]에서 Total outbreaks 문자열 들어간 위치 - index.to_list()[0]: 인덱스 타입을 리스트로 만들어 추출
    total_idx = table[0][table[0][0].str.contains('Total outbreaks')].index.to_list()[0]
    total_table_num = (total_idx - 4) * 2 + 4  # total 박스의 위치. 이 앞의 박스까지 추출함을 의미
    for n in range(4, total_table_num, 2):
        table[n].loc[0, 0] = 'Region'
    # table[n].loc[0, 0]: 'Outbreak 1'부분을 'Region'으로 바꿈. 발생 건 박스별로 칼럼 수가 다를 수 있으므로 전체 박스 다 변환
    # table[4]부터 발생 건 박스. table[n+1]: Affected animals df. 두 df를 가로로 합치는 코드를 반복
    outbreaks_df = [pd.concat([table[n].T, table[n + 1]], axis=1, ignore_index=True) for n in
                    range(4, total_table_num, 2)]
    # 박스별로 칼럼 수가 다를 수 있으므로 칼럼명에 반복문
    outbreaks_df = [outbreak_df.loc[1:].rename(columns=outbreak_df.loc[0]) for outbreak_df in outbreaks_df]
    # summary df와 outbreaks df 합치기 - sort=False: 인덱스가 같아도 아래에 그대로 붙임, ignore_index=True로 붙인 행의 인덱스를 초기화
    summary_outbreaks_df_list = [pd.concat([summary_df, outbreak_df], axis=1) for outbreak_df in outbreaks_df]
    summary_outbreaks_df = pd.concat(summary_outbreaks_df_list, sort=False, ignore_index=True)
    # 발생 건 수 넣기
    summary_outbreaks_df['Number of outbreaks'] = float('nan')
    outbreaks_number = re.search('[0-9]+', table[0].loc[3, 0]).group()  # New outbreaks(숫자)에서 숫자만 뽑아내기
    summary_outbreaks_df.loc[0, 'Number of outbreaks'] = outbreaks_number  # 첫 행에만 건 수 넣기
    
    # 필요한 열만 뽑기 - 'Affected population'와 'Report type'열은 후에 맨 뒤로 옮길 것임
    header_need = ['Report type', 'Affected population', 'Causal agent', 'Link number', 'Country', 'Agent', 'Serotype',
                   'Report date', 'Number of outbreaks', 'Date of start of the outbreak', 'Region', 'Epidemiological unit',
                   'Species', 'Susceptible', 'Cases', 'Deaths', 'Killed and disposed of', 'Slaughtered']
    
    '''
    'Serotype' or 'Affected population' or 'Causal agent' 열이 없는 리포트도 있음
        - 위에서 결합한 df에 위의 칼럼명(header_need)이 있는지 확인하는 작업
        - 없다면 해당 칼럼명의 빈 셀로 된 열을 삽입    
    '''

    for header in header_need:
        if not header in list(summary_outbreaks_df.columns):  # '포함(in)'구문을 쓰기 위해 Series 타입을 list로 바꿈
            summary_outbreaks_df[header] = float('nan')

    summary_outbreaks_df = summary_outbreaks_df[header_need]

    ## 조치사항 박스

    # 조치사항 전체 목록
    measure_header = ['Movement control inside the country', 'Vaccination in response to the outbreak (s)',
                      'Surveillance outside containment and/or protection zone',
                      'Surveillance within containment and/or protection zone', 'Screening', 'Traceability',
                      'Quarantine', 'Official destruction of animal products',
                      'Official disposal of carcasses, by-products and waste',
                      'Process to inactivate the pathogenic agent in products or by-products', 'Stamping out',
                      'Selective killing and disposal', 'Control of wildlife reservoirs', 'Zoning',
                      'Disinfection', 'Disinfestation', 'Control of vectors', 'Vector surveillance',
                      'Ante and post-mortem inspections',
                      'Vaccination permitted (if a vaccine exists)', 'Vaccination prohibited',
                      'No treatment of affected animals', 'Treatment of affected animals', 'Slaughter']

    measure_df = pd.DataFrame(columns=measure_header)  # 위 칼럼명으로 된 빈 df 만들어 놓음

    ## 합치기(summary + 발생 건 + 조치사항 df)
    report_df = pd.concat([summary_outbreaks_df, measure_df], axis=1)  # 위 빈 df와 미리 결합한 후 추가

    '''
    table[0]에서 조치사항 박스의 제목인 'Control measures'로 시작하는 행 위치와 그 텍스트
    조치사항 텍스트(measure_text)는 하나의 문자열로 되어 있음 
        - 예: 'Control of vectors  Vaccination prohibited  Measures to be applied  No other measures'
    '''
    measure_idx = table[0][table[0][0].str.startswith('Control measures')].index.to_list()[0]  # startswith: 해당 문자열로 시작
    measure_text = re.search('applied\s*(.*)', table[0].loc[measure_idx, 0]).group(1)  # 해당 리포트의 조치사항 목록

    '''
        measure_text에 조치할 사항 문구('Measures to be applied')가 들어 있다면
            - 'Measures to be applied' 문구 앞부분은 조치한 사항, 뒷부분은 조치할 사항으로 나눔
            - 안 들어 있으면 measure_text 모두 조치한 사항
    '''
    if 'Measures to be applied' in measure_text:
        measure_applied = re.search('(.*\S)\s*Measures to be applied\s*(.*)', measure_text).group(1)
        measure_tobe = re.search('(.*\S)\s*Measures to be applied\s*(.*)', measure_text).group(2)
    else:
        measure_applied = measure_text
        measure_tobe = 'No other measures'

    '''
    measure_header의 원소가 measure_applied에 들어 있으면 'T', measure_tobe에 들어 있으면 'To be'
    'Treatment of affected animals (To prevent secondary bacterial infection)'나 'Disinfection / Disinfestation'와 
    같은 변종도 잡을 수 있음
    '''
    for measure in measure_header:
        if measure in measure_applied:
            report_df[measure] = 'Y'
        elif measure_tobe != 'No other measures':  # 조치할 사항에 'No other measures'이면 스킵
            if measure in measure_tobe:
                report_df[measure] = 'To be'

    ## total 두수만 있는 df를 따로 만들기 - 두수 앞 부분(affected_before_df) + total 두수 + 조치사항
    affected_before_df = pd.DataFrame(report_df.loc[0, 'Report type': 'Epidemiological unit']).T
    total_affected_header = table[total_table_num + 1].loc[0]
    total_affected_df = table[total_table_num + 1].loc[1:].rename(columns=total_affected_header).reset_index(drop=True)
    # reset_index: 인덱스 리셋. drop=True: 기존 인덱스 버림
    total_measure_df = pd.DataFrame(report_df.loc[0, 'Movement control inside the country':]).T
    totalhead_df = pd.concat([affected_before_df, total_affected_df, total_measure_df], axis=1)

    ## 열 위치 수정. 선택한 열을 마지막으로 보냄
    report_df_header = list(report_df.columns)
    report_df_header = report_df_header[3:] + report_df_header[:3]
    report_df = report_df[report_df_header]
    totalhead_df = totalhead_df[report_df_header]  # oie_reports이나 totalhead_reports의 열은 같으므로

    return report_df, totalhead_df

if __name__ == "__main__":
    print('로딩 중입니다.\n')
    # linknumber = '31685'  # 조치사항에 표
    # linknumber = '33576'  # 지역명에 원인체명 있음. 사육 두수 빈칸
    # linknumber = '33640'  # 세부 축종 - Affected Population
    # linknumber = '33651'  # 일반. 세부 축종을 넣어보자
    # linknumber = '29041'  # 조치사항 감염동물치료괄호
    # linknumber = '29044'  # 조치사항 tobe
    # linknumber = '33716'  # 조치사항 - header에 없는 조치사항
    # linknumber = '34184'  # 총 두수에서 감염 컬럼에 *표가 있는 것
    # linknumber = '29029'  # report_df에는 구분이 있지만 totalhead_df에는 없는 것
    # linknumber = '12105'  # 아무것도 안 뜨는 것
    # linknumber = '35651'  # 원인체 없는 것

    report_df, totalhead_df = get_oie_report(linknumber)
    print('\n완료되었습니다.')