# coding = UTF-8
import re, sqlite3, pandas as pd, datetime, country_continent_sort, disease_sort
from collections import Counter


def translate_oie_reports(oie_reports, totalhead_reports):
    print('\n번역 중입니다.\n잠시만 기다려주세요.')
    # 복사. 원본 파일도 나중에 엑셀 파일로 저장할 것임
    kor_reports = oie_reports.copy()
    kor_totalhead = totalhead_reports.copy()

    # 컬럼명 한글로 재설정
    header_kor = ['링크 번호', '국가', '질병', '혈청형', '보고일', '건수', '발생일', '발생 지역', '구분', '축종', '사육', '감염', '폐사',
                  '살처분', '도축', '국내이동제한', '발생대응 예방접종', '봉쇄지역 및/또는 보호지역 외 예찰',
                  '봉쇄지역 및/또는 보호지역 내 예찰', '스크리닝', '이력 추적', '격리', '동물성 생산물 공식처리',
                  '사체·부산물·폐기물 공식처리', '생산물 또는 부산물 내 병원체 불활화 처리', '살처분*', '선택적 살처분',
                  '야생보균원 관리', '방역대 설정', '소독', '해충구제', '야생매개체 관리', '매개체 예찰', '생·해체검사',
                  '백신접종 허용(백신이 있는 경우)', '백신접종 금지', '감염동물 미치료', '감염동물 치료', '도축*', '리포트 번호', '세부 축종', '원인체']

    kor_dfs = [kor_reports, kor_totalhead]
    for kor_df in kor_dfs:
        kor_df.columns = header_kor

        # 빈 셀을 nan로 채움
        kor_df.fillna(float('nan'), inplace=True)  # Nonetype값을 nan로 바꿈
        kor_df.replace('', float('nan'), inplace=True)  # ''을 nan으로 바꿈

        ## 숫자 문자열 int화
        def int_convert(df, col):
            for num, value in enumerate(df[col]):  # num: 순서 - 해당 열의 인덱스에 해당
                if type(value) == str:  # float('nan')은 int로 바꿀 수 없음
                    try:
                        df.loc[num, col] = int(value)  # 해당 열의 0행 값부터 마지막행 값까지 하나하나씩 int로 변환
                    except ValueError:
                        number_extract_value = re.search('[0-9]*', value).group()  # 값에 특수기호(*)가 들어 있어서 숫자만 추출
                        if number_extract_value == '':  # 값에 *만 있고 숫자가 없는 경우는 ''만 남음
                            df.loc[num, col] = float('nan')  # ''를 nan로 바꿈
                        else:
                            df.loc[num, col] = int(number_extract_value)  # 추출한 숫자 문자열을 int화
        '''
        * 지금까지 이 곳에서 오류가 난 이유
        1.몇몇 곳의 값이 ''였음 - kor_df.replace를 이용해 모든 ''을 nan로 바꿈. 근데 왜 ''에서 숫자만 추출하는 건 안 될까
        2.df의 해당 열에 int화한 리스트를 그대로 대입하면 int가 float로 바뀌는 신기한 상황이 발생 - 해당 열의 값 하나하나씩을 int로 변환
        '''
        int_columns = ['링크 번호', '건수', '사육', '감염', '폐사', '살처분', '도축']
        for int_column in int_columns:
            int_convert(df=kor_df, col=int_column)

        ## 날짜 문자열 date화
        def date_convert(df, col):  # dd/mm/yy를 yy-mm-dd로 변환
            for value in df[col]:
                if type(value) == str:  # 빈 값(nan값)은 제외하기 위함
                    # dd/mm/yy 문자열을 날짜 객체로 변환 - yy-mm-dd 형식으로 변환됨
                    df.loc[df[col] == value, col] = datetime.datetime.strptime(value, '%d/%m/%Y').date()

        date_columns = ['보고일', '발생일']
        for date_col in date_columns:
            date_convert(df=kor_df, col=date_col)


        ## 나머지 값 한글화
        # 한글화 리스트에서 찾아서 바꿀 때
        '''df의 col열에서, 값이 eng_list의 n번째 원소와 같으면 kor_list의 n번째 원소로 변환'''
        def kor_find_convert(df, col, eng_list, kor_list):
            for n in range(len(eng_list)):
                df.loc[df[col] == eng_list[n], col] = kor_list[n]

        # 특정 한글 단어로 바꿀 때
        '''
        df의 col1열에서, 값이 search_list1의 원소와 같으면 kor1로 변환
        df의 col2열에서, 값이 search_list2의 원소와 같으면 kor2로 변환
        '''
        def kor_convert(df, col1, col2, search_list1, search_list2, kor1, kor2):
            for search1 in search_list1:
                df.loc[df[col1] == search1, col2] = kor1
            for search2 in search_list2:
                df.loc[df[col1] == search2, col2] = kor2

        # 국가명 한글화 - country_continent_sort.py에서 가져옴
        country_continent_df = country_continent_sort.country_continent_df
        country_eng_list = country_continent_df['Country'].to_list()  # 국가 영문명 리스트
        country_kor_list = country_continent_df['국가'].to_list()  # 국가 한글명 리스트
        continent_kor_list = country_continent_df['대륙'].to_list()  # 대륙 한글명 리스트

        kor_find_convert(df=kor_df, col='국가', eng_list=country_eng_list, kor_list=country_kor_list)
        # 대륙명 넣기
        kor_df['지역'] = float('nan')  # '지역'열 생성. 지역은 대륙을 뜻함
        '''
        kor_df '국가'열의 값을 country_kor_list에서 찾아, 그 위치의 continent_kor_list의 원소를 kor_df '지역'열에 넣음
        값이 str일 경우에만 돌게끔
        try except로 만약 값이 없을 시 회피 
        '''
        for country_kor in kor_df['국가']:
            if type(country_kor) == str:
                try: kor_df.loc[kor_df['국가'] == country_kor, '지역'] = continent_kor_list[country_kor_list.index(country_kor)]
                except: kor_df.loc[kor_df['국가'] == country_kor, '지역'] = '미결정'
        # 질병명 한글화
        disease_eng_list = disease_sort.disease_eng
        disease_kor_list = disease_sort.disease_kor
        kor_find_convert(df=kor_df, col='질병', eng_list=disease_eng_list, kor_list=disease_kor_list)
        # 축종명 한글화
        species_eng = ['Swine', 'Wild boar:Sus scrofa(Suidae)', 'Dogs', 'Cats', 'Cattle', 'Equidae', 'Sheep', 'Goats',
                       'Sheep / goats', 'Bees (hives)', 'Birds', 'Buffaloes', 'Camelidae', 'Rabbits']
        species_kor = ['돼지', '멧돼지', '개', '고양이', '소', '말과', '면양', '산양', '면양/산양', '꿀벌', '조류', '버팔로',
                       '낙타과', '토끼']
        kor_find_convert(df=kor_df, col='축종', eng_list=species_eng, kor_list=species_kor)

        # # 사육 두수로 구분'열 값 분류
        # kor_df.loc[kor_df['사육'] != float('nan'), '구분'] = '사육'

        
        # '구분'열 한글화
        stock_place_list = ['Apiary', 'Backyard', 'Farm', 'Livestock market', 'Slaughterhouse', 'Village', 'Zoo']
        wild_place_list = ['Forest', 'Natural park']
        kor_convert(df=kor_df, col1='구분', col2='구분', search_list1=stock_place_list, search_list2=wild_place_list, kor1='사육',
                    kor2='야생')

        # 축종으로 '구분'열 값 분류
        stock_species = ['돼지']
        wild_species = ['멧돼지']
        kor_convert(df=kor_df, col1='축종', col2='구분', search_list1=stock_species, search_list2=wild_species, kor1='사육',
                    kor2='야생')

        # 리포트 번호 추출 및 리포트 구분 한글화
        kor_df['리포트 구분'] = float('nan')  # '리포트 구분'열 생성
        for value in kor_df['리포트 번호']:
            if type(value) == str:
                try:  # 숫자만 추출
                    followup_number = int(re.search('[0-9]+', value).group())
                    kor_df.loc[kor_df['리포트 번호'] == value, '리포트 번호'] = followup_number
                    kor_df.loc[kor_df['리포트 번호'] == followup_number, '리포트 구분'] = '추가'
                except:  # 숫자가 없다면 '긴급'을 반환
                    kor_df.loc[kor_df['리포트 번호'] == value, ['리포트 번호', '리포트 구분']] = '긴급'

    ## 요청에 맞게 컬럼 위치 수정
    header_kor_modi = ['링크 번호', '질병', '지역', '국가'] + header_kor[3:] + ['리포트 구분']
    kor_reports_df = kor_reports[header_kor_modi].copy()  # 기존 df에 가공을 하면 경고 메시지가 떠서 .copy()로 회피
    kor_totalhead_df = kor_totalhead[header_kor_modi].copy()
    dfs = [kor_reports_df, kor_totalhead_df]
    del dfs[0]['리포트 구분']  # kor_reports_df에는 해당 열이 필요없어서 삭제

    ## '링크 번호'열을 이용해 kor_totalhead df의 '발생일'열과 '구분'열의 값을 변환
    '''
    kor_reports_df에서 '링크 번호'열 값의 중복 수가 1개를 넘으면
    kor_totalhead_df에서 그 '링크 번호'행의 '발생일'열 값에 "+ '~' + kor_reports_df에서 그 '링크 번호'행의 마지막 '발생일' 열 값"
    을 추가
    '''
    # Counter: 각 값의 개수 세는 메서드. dropna(): '링크 번호'가 없는 행 제거
    dfs[1]['구분*'] = float('nan')  # '구분*'열 생성
    # 해당 링크 번호의 변환할 열의 값을 추출하는 함수를 만듦. reset_index(drop=True): 추출한 series의 인덱스 초기화
    want_linknumber_loc = lambda df_list, col1, col2: \
        [df_list[n].loc[df_list[n][col1] == want_linknumber, col2].reset_index(drop=True) for n in range(len(df_list))]
    # 해당 링크 번호의 발생일 값을 변환
    for want_linknumber in dfs[1]['링크 번호'].dropna():  # kor_totalhead_df의 링크 번호 열 반복문(nan 제거)
        '''
        kor_reports df에서 '구분'열에 '사육'과 '야생'이 모두 있으면, kor_totalhead df의 '구분*'열에 '사육/야생', '사육'만 있으면 '사육',
        '야생'만 있으면 '야생'을 넣음
        '야생' 혹은 '사육'으로 변환 안 된 값이 있는 '구분'행에 '사육'이 있으면 '사육'으로 반환하게 됨
            - 만약 변환 안 된 그 값이 '야생'이면 '사육/야생'을 반환해야 함
            - 따라서 위의 값은 '미결정'으로 반환
        '''
        species_list = want_linknumber_loc(df_list=dfs, col1='링크 번호', col2='구분')
        if 'Not applicable' in list(species_list[0]) or 'Other' in list(species_list[0]):
            # 이 코드는 나중에 구분에서 사육/야생으로 다 번역되면 필요없음. 구분 값 'Not applicable', 'Other'는 아직 번역이 안 됨
            dfs[1].loc[dfs[1]['링크 번호'] == want_linknumber, '구분*'] = '미결정'
        elif '사육' in list(species_list[0]) and '야생' in list(species_list[0]):
            dfs[1].loc[dfs[1]['링크 번호'] == want_linknumber, '구분*'] = '사육/야생'
        elif '사육' in list(species_list[0]):
            dfs[1].loc[dfs[1]['링크 번호'] == want_linknumber, '구분*'] = '사육'
        elif '야생' in list(species_list[0]):
            dfs[1].loc[dfs[1]['링크 번호'] == want_linknumber, '구분*'] = '야생'

    # kor_totalhead_df에서 '건수'열의 값이 1 이상인 행은 '발생 지역'열 값에 문자열 ' 등'을 추가
    dfs[1].loc[dfs[1]['건수'] > 1, '발생 지역'] = dfs[1].loc[dfs[1]['건수'] > 1, '발생 지역'] + ' 등'

    return oie_reports, kor_reports_df, kor_totalhead_df

if __name__ == '__main__':
    print('로딩 중입니다.\n')
    # db에서 원하는 부분 가져오기 - 원하는 데이터만 가져와 한글화가 필요할 때
    import os
    '''
    'oiescraper' 폴더에 db파일이 있음
    현재 경로에 db파일이 있으면(현재 경로가 'oiescraper' 폴더라면) 그대로, 없으면 'oiescraper' 폴더로 경로 설정
    '''
    if 'oie_reports.db' in os.listdir():  # os.listdir(path): path의 하위 폴더 및 파일 리스트. path 넣지 않으면 현재 경로로 설정
        con = sqlite3.connect('oie_reports.db')
    else:
        con = sqlite3.connect('./oiescraper/oie_reports.db')  # doc(.): 현재 경로
    # # 전체 db
    # oie_reports = pd.read_sql("SELECT * FROM oie_reports", con)  # FROM: 해당 테이블, SELECT: 데이터 가져오기
    # totalhead_reports = pd.read_sql("SELECT * FROM totalhead_reports", con)  # *: 전체 열 가져오기
    # # 한달 치
    # oie_reports = pd.read_sql("SELECT * FROM oie_reports WHERE rowid BETWEEN 13765 AND 13837", con)  # rowid: 행
    # totalhead_reports = pd.read_sql("SELECT * FROM totalhead_reports WHERE rowid BETWEEN 2685 AND 2709", con)
    # # 국가가 한국인 것
    # oie_reports = pd.read_sql("SELECT * FROM oie_reports WHERE Country LIKE '%korea (rep. of)'", con)
    # totalhead_reports = pd.read_sql("SELECT * FROM totalhead_reports WHERE Country LIKE '%korea (rep. of)'", con)
    # # 헝가리, 폴란드 hpai 조회
    # oie_reports = pd.read_sql("SELECT * FROM oie_reports WHERE Country IN ('Hungary', 'Poland') AND Agent LIKE 'Highly pathogenic%'", con)
    # totalhead_reports = pd.read_sql("SELECT * FROM totalhead_reports WHERE Country IN ('Hungary', 'Poland') AND Agent LIKE 'Highly pathogenic%'", con)

    # 베트남 asf 조회
    oie_reports = pd.read_sql("SELECT * FROM oie_reports WHERE Country = 'Vietnam' AND Agent = 'African swine fever'", con)
    totalhead_reports = pd.read_sql("SELECT * FROM totalhead_reports WHERE Country = 'Vietnam' AND Agent = 'African swine fever'", con)

    con.close()

    trans_dfs = translate_oie_reports(oie_reports, totalhead_reports)
    kor_oie = trans_dfs[1]
    kor_total = trans_dfs[2]
    # print('\n완료되었습니다.\n만들어진 엑셀 파일을 확인하세요.')  # 개수 입력 후 콘솔에 보여질 문구
    # input('엔터키를 누르면 종료합니다.')  # 엔터 치면 종료되게끔
