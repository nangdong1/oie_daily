# coding = UTF-8
print('로딩 중입니다.\n')
import os, time, requests, re, sqlite3, pandas as pd, warnings, urllib.request
from bs4 import BeautifulSoup
from tqdm import tqdm  # 진행표시줄
from oiescraper_sub_ver3 import get_oie_report
from oiescraper_kor_ver5 import translate_oie_reports
# from selenium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC

def get_oie_data():
    ## 각 report의 링크를 목록화
    url = 'https://www.oie.int/wahis_2/public/wahid.php/Diseaseinformation/WI/index/newlang/en'  # oie report 리스트의 url

    # response = requests.get(url, verify=False)
    # soup = BeautifulSoup(response.content, 'html.parser')  # BeautifulSoup을 이용해 html 분석
    with urllib.request.urlopen(url) as response:
        soup = BeautifulSoup(response.read(), 'lxml')

    tags = soup.select('table a')  # 원하는 자료가 table 태그 아래 a 태그에 속해 있음
    hrefs = [tag['href'] for tag in tags]  # 속성 href의 값 추출. href는 링크를 뜻함
    linknumbers = [re.search('[0-9]{5}', href).group() for href in hrefs]
    # url은 이미 알고 있기 때문에 굳이 link 뽑을 필요 없이 유일하게 변하는 부분인 링크번호만 뽑음

    ## 새로 올라온 리포트만 추출하기
    number = 150  # 기본 150개의 리포트를 추출
    want_linknumbers = [linknumbers[n] for n in reversed(range(number))]

    # # sql db에 연결 및 로딩 오류 회피 위한 chromedriver 사용
    # chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_argument('headless')
    # chrome_options.add_argument('disable-gpu')

    '''
    'oiescraper' 폴더에 db파일이 있음
    현재 경로에 db파일이 있으면(현재 경로가 'oiescraper' 폴더라면) 그대로, 없으면 'oiescraper' 폴더로 경로 설정
    '''
    if 'oie_reports.db' in os.listdir():  # os.listdir(path): path의 하위 폴더 및 파일 리스트. path 넣지 않으면 현재 경로로 설정
        con = sqlite3.connect('oie_reports.db')
        # driver = webdriver.Chrome('chromedriver.exe', options=chrome_options)  # 크롬 드라이버 실행파일 위치 넣음

    else:
        con = sqlite3.connect('./oiescraper/oie_reports.db')  # doc(.): 현재 경로
        # driver = webdriver.Chrome('./oiescraper/chromedriver.exe', options=chrome_options)  # 크롬 드라이버 실행파일 위치 넣음
    # 기존 db의 링크 넘버 df 불러옴
    linknumber_db_df = pd.read_sql('SELECT [Link number] FROM totalhead_reports', con)  # []: 칼럼명이나 테이블명에 공백이 있을 때

    '''원하는 링크번호를 추출한 리스트와 linknumber_db_df를 대조하여 새로운 링크번호만 추출 후 get_oie_report 함수에 넣어 데이터 추출'''

    new_linknumbers = []  # 새로운 링크번호 모음을 위한 빈 리스트
    for want_linknumber in want_linknumbers:
        if not want_linknumber in list(linknumber_db_df['Link number']):  # 링크넘버 db에 원하는 링크번호가 없다면
            new_linknumbers.append(want_linknumber)  # 새로운 링크넘버 리스트에 추가
    # 새로운 링크번호 하나하나씩 get_oie_report(linknumber) 실행
    print('데이터 추출 중입니다.')
    oie_reports = pd.DataFrame()  # 리포트의 빈 df. 새로운 리포트 추가하기 위함
    totalhead_reports = pd.DataFrame()  # total 두수의 빈 df
    error_linknumbers = []  # 링크번호 에러 리스트

    for new_linknumber in tqdm(new_linknumbers):  # tqdm: 반복문에서의 리스트 원소 수를 백분율로 나타내어 진행표시줄을 만듦

        # report_url = f'http://www.oie.int/wahis_2/public/wahid.php/Reviewreport/Review?page_refer=MapFullEventReport&reportid={new_linknumber}'
        # driver.get(report_url)

        # try:
        #     WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.CLASS_NAME, 'MidBigTable')))
        # except:
        #     error_linknumbers.append(new_linknumber)
        #     continue

        try:
            oie_report, totalhead_report = get_oie_report(new_linknumber)  # get_oie_report(linknumber)의 반환값이 튜플 형태임
        except:
            error_linknumbers.append(new_linknumber)
            continue
        
        # 추출한 데이터 및 링크번호 추가
        '''
        append
            - 리스트는 list.append(x), df는 df = df.append(x, ignore_index=True)
            _ df는 왜 다시 변수를 지정해 줘야 할까
        '''
        oie_reports = oie_reports.append(oie_report, ignore_index=True)  # df.append 사용 시 무조건 ignore_index=True!
        totalhead_reports = totalhead_reports.append(totalhead_report, ignore_index=True)

        time.sleep(3)  # 작업을 멈춤. 초 단위

        # driver.implicitly_wait(15)

    # driver.close()

    '''추출한 데이터가 하나도 없을 때 넘어가는 코드. oie_reports의 데이터가 하나도 없으면 print() 아니면 db에 저장 후 한글화'''
    if oie_reports.empty == True:
        print('\n발생 건이 없습니다.')

    ## 기존 sql의 DB에 추가하기
    else:
        warnings.filterwarnings(action='ignore')  # 경고 메시지 안 띄움. action='ignore': 무시, 'default': 경고 발생 위치에 출력

        # 한글화
        trans_dfs = translate_oie_reports(oie_reports, totalhead_reports)

        # 추출한 엑셀파일을 바탕화면에 저장
        desktop_path = f'C:\\Users\\{os.getlogin()}\\Desktop\\'
        
        with pd.ExcelWriter(desktop_path + 'oie_reports.xlsx') as writer:
            trans_dfs[0].to_excel(writer, '원본', index=False)
            trans_dfs[1].to_excel(writer, '번역본', index=False)
            trans_dfs[2].to_excel(writer, '해동(총 두수)', index=False)

        oie_reports.to_sql('oie_reports', con, if_exists='append', index=False)
        totalhead_reports.to_sql('totalhead_reports', con, if_exists='append', index=False)
        # if_exists: 해당 테이블명이 이미 있을 때
        # - 'fail': 작업 없음(기본값), 'append': 기존 테이블에 데이터 추가, 'replace': 기존 테이블 삭제

        print('\n완료되었습니다.\n만들어진 엑셀 파일을 확인하세요.')  # 개수 입력 후 콘솔에 보여질 문구
    con.close()
    os.system('Pause')  # 커맨드라인에서 자동으로 종료되지 않고 멈춤

    # return error_linknumbers

    return oie_reports, totalhead_reports, error_linknumbers

if __name__ == "__main__":

    # os.chdir('C:/Users/Nangdong/Desktop/py_works/oie_works/for_git/oiescraper')
    oie_reports, totalhead_reports, error_linknumbers = get_oie_data()
